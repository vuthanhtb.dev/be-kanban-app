package model

import "be-kanban-app/common"

type Board struct {
	common.SQLModel   `json:",inline"`
	Title             string `json:"title" gorm:"column:title;default:Untitled"`
	Icon              string `json:"icon" gorm:"column:icon;default:📃"`
	Description       string `json:"description" gorm:"column:description;default:Add description here"`
	Position          int    `json:"position" gorm:"column:position"`
	Favourite         bool   `json:"favourite" gorm:"column:favourite;default:false"`
	FavouritePosition int    `json:"favourite_position" gorm:"column:favourite_position;default:0"`
	UserId            int    `json:"user_id" gorm:"column:user_id"`
}

func (Board) TableName() string {
	return "boards"
}
