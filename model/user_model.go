package model

import "be-kanban-app/common"

type User struct {
	common.SQLModel `json:",inline"`
	UserName        string `json:"username" gorm:"column:username"`
	Password        string `json:"password" gorm:"column:password"`
	Salt            string `json:"-" gorm:"column:salt;"`
	//Boards          []Board `json:"boards" gorm:"foreignKey:UserId;PRELOAD:false;"`
}

func (User) TableName() string {
	return "users"
}
