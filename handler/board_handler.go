package handler

import (
	"be-kanban-app/business"
	"be-kanban-app/common"
	"be-kanban-app/middleware"
	"be-kanban-app/model"
	"be-kanban-app/storage"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func BoardHandler(router *gin.Engine, appContext common.AppContext) {
	store := storage.NewSQLStore(appContext.GetMainDBConnection())

	boardBusiness := business.NewBoardBusiness(store)

	auth := router.Group("/api/v1/boards", middleware.RequiredAuth(appContext, store))
	{
		auth.POST("/", CreateBoardHandler(boardBusiness))
		auth.GET("/", GetAllBoardHandler(boardBusiness))
		auth.GET("/:id", FindBoardHandler(boardBusiness))
		auth.GET("/favourites", GetFavouritesHandler(boardBusiness))
		auth.PUT("/favourites", UpdateFavouritePositionHandler(boardBusiness))
	}
}

func GetFavouritesHandler(boardBusiness business.BoardBusiness) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		result, err := boardBusiness.GetAllBoardBusiness(ctx.Request.Context())
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		ctx.JSON(http.StatusOK, common.SimpleSuccessResponse(result))
	}
}

func UpdateFavouritePositionHandler(boardBusiness business.BoardBusiness) gin.HandlerFunc {
	return func(ctx *gin.Context) {

		ctx.JSON(http.StatusOK, gin.H{"message": ""})
	}
}

func GetAllBoardHandler(boardBusiness business.BoardBusiness) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		result, err := boardBusiness.GetAllBoardBusiness(ctx.Request.Context())
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		ctx.JSON(http.StatusOK, common.SimpleSuccessResponse(result))
	}
}

func FindBoardHandler(boardBusiness business.BoardBusiness) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		id, err := strconv.Atoi(ctx.Param("id"))

		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		result, err := boardBusiness.FindBoardBusiness(ctx.Request.Context(), id)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		ctx.JSON(http.StatusOK, common.SimpleSuccessResponse(result))
	}
}

func CreateBoardHandler(boardBusiness business.BoardBusiness) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var data model.Board

		if err := ctx.ShouldBind(&data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		if err := boardBusiness.CreateBoardBusiness(ctx.Request.Context(), &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		ctx.JSON(http.StatusOK, gin.H{"message": "register success"})
	}
}
