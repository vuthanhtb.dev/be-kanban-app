package handler

import (
	"be-kanban-app/business"
	"be-kanban-app/common"
	"be-kanban-app/common/hasher"
	"be-kanban-app/common/tokenprovider/jwt"
	"be-kanban-app/model"
	"be-kanban-app/storage"
	"github.com/gin-gonic/gin"
	"net/http"
)

func AuthHandler(router *gin.Engine, appContext common.AppContext) {
	store := storage.NewSQLStore(appContext.GetMainDBConnection())

	md5 := hasher.NewMd5Hash()
	tokenProvider := jwt.NewTokenJWTProvider(appContext.SecretKey())

	userBusiness := business.NewUserBusiness(store, tokenProvider, md5, 60*60*24*30)

	auth := router.Group("/api/v1/auth")
	{
		auth.POST("/register", registerHandler(userBusiness))
		auth.POST("/login", loginHandler(userBusiness))
	}
}

func loginHandler(userBusiness business.UserBusiness) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var data model.User

		if err := ctx.ShouldBind(&data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		account, err := userBusiness.LoginBusiness(ctx.Request.Context(), &data)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		ctx.JSON(http.StatusOK, common.SimpleSuccessResponse(account))
	}
}

func registerHandler(userBusiness business.UserBusiness) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var data model.User

		if err := ctx.ShouldBind(&data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		if err := userBusiness.RegisterBusiness(ctx.Request.Context(), &data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		ctx.JSON(http.StatusOK, gin.H{"message": "register success"})
	}
}
