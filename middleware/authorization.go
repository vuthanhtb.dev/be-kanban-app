package middleware

import (
	"be-kanban-app/common"
	"be-kanban-app/common/tokenprovider/jwt"
	"be-kanban-app/model"
	"context"
	"errors"
	"github.com/gin-gonic/gin"
	"strings"
)

type AuthenticationStore interface {
	FindUser(ctx context.Context, conditions map[string]interface{}, moreInfo ...string) (*model.User, error)
}

func extractTokenFromHeaderString(s string) (string, error) {
	parts := strings.Split(s, " ")

	if parts[0] != "Bearer" || len(parts) < 2 || strings.TrimSpace(parts[1]) == "" {
		return "", errors.New("wrong authentication header")
	}

	return parts[1], nil
}

func RequiredAuth(appContext common.AppContext, authStore AuthenticationStore) func(c *gin.Context) {
	tokenProvider := jwt.NewTokenJWTProvider(appContext.SecretKey())

	return func(c *gin.Context) {
		token, err := extractTokenFromHeaderString(c.GetHeader("Authorization"))

		if err != nil {
			panic(err)
		}

		payload, err := tokenProvider.Validate(token)
		if err != nil {
			panic(err)
		}

		user, err := authStore.FindUser(c.Request.Context(), map[string]interface{}{"id": payload.UserId})

		if err != nil {
			panic(err)
		}

		c.Set(common.CurrentUser, user)
		c.Next()
	}
}
