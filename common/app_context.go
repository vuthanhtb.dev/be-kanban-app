package common

import "gorm.io/gorm"

type AppContext interface {
	GetMainDBConnection() *gorm.DB
	SecretKey() string
}

type appContext struct {
	db     *gorm.DB
	secret string
}

func NewAppContext(db *gorm.DB, secret string) *appContext {
	return &appContext{db: db, secret: secret}
}

func (ctx *appContext) GetMainDBConnection() *gorm.DB {
	return ctx.db.Debug()
}

func (ctx *appContext) SecretKey() string {
	return ctx.secret
}
