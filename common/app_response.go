package common

import "net/http"

var (
	SimpleSuccessResponse = func(data interface{}) Response {
		return newResponse(http.StatusOK, data, nil, nil)
	}
)

type Response struct {
	Code   int         `json:"code"`
	Data   interface{} `json:"data"`
	Param  interface{} `json:"param,omitempty"`
	Paging interface{} `json:"paging,omitempty"`
}

func newResponse(code int, data, param, other interface{}) Response {
	return Response{
		Code:   code,
		Data:   data,
		Param:  param,
		Paging: other,
	}
}
