package business

import (
	"be-kanban-app/common"
	"be-kanban-app/common/tokenprovider"
	"be-kanban-app/model"
	"context"
	"errors"
)

type UserStorage interface {
	RegisterStore(ctx context.Context, data *model.User) error
	FindUser(ctx context.Context, conditions map[string]interface{}, moreInfo ...string) (*model.User, error)
}

type Hasher interface {
	Hash(data string) string
}

type userBusiness struct {
	userStorage   UserStorage
	tokenProvider tokenprovider.Provider
	hasher        Hasher
	expiry        int
}

type UserBusiness interface {
	RegisterBusiness(ctx context.Context, data *model.User) error
	LoginBusiness(ctx context.Context, data *model.User) (*tokenprovider.Token, error)
}

func NewUserBusiness(userStorage UserStorage, tokenProvider tokenprovider.Provider, hasher Hasher, expiry int) *userBusiness {
	return &userBusiness{
		userStorage:   userStorage,
		tokenProvider: tokenProvider,
		hasher:        hasher,
		expiry:        expiry,
	}
}

func (business *userBusiness) RegisterBusiness(ctx context.Context, data *model.User) error {
	user, _ := business.userStorage.FindUser(ctx, map[string]interface{}{"username": data.UserName})

	if user != nil {
		return errors.New("user existed")
	}

	salt := common.GenSalt(50)

	data.Password = business.hasher.Hash(data.Password + salt)
	data.Salt = salt

	if err := business.userStorage.RegisterStore(ctx, data); err != nil {
		return errors.New("register failed")
	}

	return nil
}

func (business *userBusiness) LoginBusiness(ctx context.Context, data *model.User) (*tokenprovider.Token, error) {
	user, err := business.userStorage.FindUser(ctx, map[string]interface{}{"username": data.UserName})

	if err != nil {
		return nil, errors.New("UserName or Password invalid")
	}

	passHashed := business.hasher.Hash(data.Password + user.Salt)

	if user.Password != passHashed {
		return nil, errors.New("UserName or Password invalid")
	}

	payload := tokenprovider.TokenPayload{
		UserId: user.Id,
	}

	accessToken, err := business.tokenProvider.Generate(payload, business.expiry)
	if err != nil {
		return nil, errors.New("something went wrong in the server")
	}

	return accessToken, nil
}
