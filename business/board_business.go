package business

import (
	"be-kanban-app/model"
	"context"
	"errors"
)

type BoardStorage interface {
	FindBoard(ctx context.Context, conditions map[string]interface{}, moreInfo ...string) (*model.Board, error)
	GetAllBoardStore(ctx context.Context, condition map[string]interface{}, moreInfo ...string) ([]model.Board, error)
	CreateBoardStore(ctx context.Context, data *model.Board) error
}

type boardBusiness struct {
	boardStorage BoardStorage
}

type BoardBusiness interface {
	CreateBoardBusiness(ctx context.Context, data *model.Board) error
	FindBoardBusiness(ctx context.Context, id int) (*model.Board, error)
	GetAllBoardBusiness(ctx context.Context) ([]model.Board, error)
}

func NewBoardBusiness(userStorage BoardStorage) *boardBusiness {
	return &boardBusiness{
		boardStorage: userStorage,
	}
}

func (business *boardBusiness) CreateBoardBusiness(ctx context.Context, data *model.Board) error {
	if err := business.boardStorage.CreateBoardStore(ctx, data); err != nil {
		return errors.New("create failed")
	}

	return nil
}

func (business *boardBusiness) FindBoardBusiness(ctx context.Context, id int) (*model.Board, error) {
	board, err := business.boardStorage.FindBoard(ctx, map[string]interface{}{"id": id})

	if err != nil {
		return nil, errors.New(err.Error())
	}

	return board, nil
}

func (business *boardBusiness) GetAllBoardBusiness(ctx context.Context) ([]model.Board, error) {
	result, err := business.boardStorage.GetAllBoardStore(ctx, nil)

	if err != nil {
		return nil, errors.New(err.Error())
	}

	return result, nil
}

func (business *boardBusiness) GetFavouriteBusiness(ctx context.Context, userId int) ([]model.Board, error) {
	result, err := business.boardStorage.GetAllBoardStore(ctx, map[string]interface{}{"user_id": userId, "favourite": true})

	if err != nil {
		return nil, errors.New(err.Error())
	}

	return result, nil
}
