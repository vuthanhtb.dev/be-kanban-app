package main

import (
	"be-kanban-app/common"
	"be-kanban-app/handler"
	"be-kanban-app/model"
	"github.com/gin-gonic/gin"
	cors "github.com/itsjamie/gin-cors"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
	"net/http"
	"os"
	"time"
)

func main() {
	secretKey := os.Getenv("SECRET_KEY")
	dsn := os.Getenv("DB_CONN_STR")
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Fatalln("Cannot connect to MySQL:", err)
	}

	if err := db.AutoMigrate(&model.User{}, &model.Board{}); err != nil {
		log.Fatalln(err.Error())
	}

	router := gin.Default()
	router.Use(cors.Middleware(cors.Config{
		Origins:         "*",
		Methods:         "GET, PUT, POST, DELETE",
		RequestHeaders:  "Origin, Authorization, Content-Type",
		ExposedHeaders:  "",
		MaxAge:          50 * time.Hour,
		Credentials:     false,
		ValidateHeaders: false,
	}))

	router.GET("/ping", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, gin.H{"message": "ping"})
	})

	appContext := common.NewAppContext(db, secretKey)
	handler.AuthHandler(router, appContext)
	handler.BoardHandler(router, appContext)

	if err := router.Run(":9300"); err != nil {
		log.Fatalln("Server error:" + err.Error())
	}
}
