package storage

import (
	"be-kanban-app/model"
	"context"
	"errors"
	"gorm.io/gorm"
)

func (s *sqlStore) FindBoard(_ context.Context, conditions map[string]interface{}, moreInfo ...string) (*model.Board, error) {
	db := s.db.Table(model.Board{}.TableName())

	for i := range moreInfo {
		db = db.Preload(moreInfo[i])
	}

	var board model.Board

	if err := db.Where(conditions).First(&board).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("record not found")
		}

		return nil, errors.New("something went wrong with DB")
	}

	return &board, nil
}

func (s *sqlStore) GetAllBoardStore(_ context.Context, condition map[string]interface{}, moreInfo ...string) ([]model.Board, error) {
	db := s.db.Table(model.Board{}.TableName())

	for i := range moreInfo {
		db = db.Preload(moreInfo[i])
	}

	var boards []model.Board

	if err := db.Where(condition).Find(&boards).Error; err != nil {
		return nil, errors.New("something went wrong with DB")
	}

	return boards, nil
}

func (s *sqlStore) CreateBoardStore(_ context.Context, data *model.Board) error {
	db := s.db.Begin()
	data.PrepareForInsert()

	if err := db.Table(data.TableName()).Create(data).Error; err != nil {
		db.Rollback()
		return errors.New("something went wrong with DB")
	}

	if err := db.Commit().Error; err != nil {
		db.Rollback()
		return errors.New("something went wrong with DB")
	}

	return nil
}
