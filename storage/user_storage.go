package storage

import (
	"be-kanban-app/model"
	"context"
	"errors"
	"gorm.io/gorm"
)

func (s *sqlStore) RegisterStore(_ context.Context, data *model.User) error {
	db := s.db.Begin()
	data.PrepareForInsert()

	if err := db.Table(data.TableName()).Create(data).Error; err != nil {
		db.Rollback()
		return errors.New("something went wrong with DB")
	}

	if err := db.Commit().Error; err != nil {
		db.Rollback()
		return errors.New("something went wrong with DB")
	}

	return nil
}

func (s *sqlStore) FindUser(_ context.Context, conditions map[string]interface{}, moreInfo ...string) (*model.User, error) {
	db := s.db.Table(model.User{}.TableName())

	for i := range moreInfo {
		db = db.Preload(moreInfo[i])
	}

	var user model.User

	if err := db.Where(conditions).First(&user).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("record not found")
		}

		return nil, errors.New("something went wrong with DB")
	}

	return &user, nil
}
